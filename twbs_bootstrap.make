api = 2
core = 7.x

; Libraries
libraries[bootstrap][directory_name] = bootstrap
libraries[bootstrap][download][type] = file
libraries[bootstrap][download][url] = https://github.com/twbs/bootstrap/releases/download/v3.2.0/bootstrap-3.2.0-dist.zip
libraries[bootstrap][type] = library
